<?php

class MatrizesSeeder extends Seeder {

    public function run()
    {
        DB::table('abrangencia')->delete();

        $data = array(
            array(
                'matriz'             => 'Matriz',
                'matrizes_regionais' => 'Regionais'
            )
        );

        DB::table('abrangencia')->insert($data);
    }

}
