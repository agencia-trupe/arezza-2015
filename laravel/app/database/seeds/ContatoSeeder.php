<?php

class ContatoSeeder extends Seeder {

    public function run()
    {
        DB::table('contato')->delete();

        $data = array(
            array(
                'telefone'          => '0800 777 4126',
                'email'             => 'arezza@arezza.com.br',
                'assessoria_titulo' => 'Nome da Empresa de Assessoria',
                'assessoria_info'   => 'João da Silva',
                'facebook'          => 'facebook',
                'twitter'           => 'twitter',
                'youtube'           => 'youtube',
            )
        );

        DB::table('contato')->insert($data);
    }

}
