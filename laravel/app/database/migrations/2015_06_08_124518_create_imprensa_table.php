<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImprensaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('imprensa', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('categoria_id');
			$table->string('data');
			$table->string('autor');
			$table->string('titulo');
			$table->string('slug');
			$table->text('olho');
			$table->text('texto');
			$table->string('capa');
			$table->string('video');
			$table->string('pdf')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('imprensa');
	}

}
