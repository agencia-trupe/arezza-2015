<?php

// Front-end

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::get('empresa/{slug?}', ['as' => 'empresa', 'uses' => 'EmpresaController@index']);

Route::get('mao-de-obra/{slug?}/{interna?}', ['as' => 'mao-de-obra', 'uses' => 'MaoDeObraController@index']);

Route::get('cases/{id?}', ['as' => 'cases', 'uses' => 'CasesController@index']);

Route::get('vagas', ['as' => 'vagas', 'uses' => 'VagasController@index']);
Route::post('vagas', ['as' => 'vagas.envio', 'uses' => 'VagasController@envio']);

Route::get('imprensa/filtro/{categoria_id}', ['as' => 'imprensa.filtro', 'uses' => 'ImprensaController@filtro']);
Route::get('imprensa/{slug?}', ['as' => 'imprensa', 'uses' => 'ImprensaController@index']);

Route::get('contato', ['as' => 'contato', 'uses' => 'ContatoController@index']);
Route::post('contato', ['as' => 'contato.envio', 'uses' => 'ContatoController@envio']);


// Download CV

Route::get('download/{key}', ['as' => 'download', function($key){
    $curriculo = Curriculo::download($key)->first();
    if (!$curriculo) App::abort('404');

    return Response::download(base_path('curriculos/'.$curriculo->curriculo));
}]);


// Painel

Route::get('painel', [
    'before' => 'auth',
    'as'     => 'painel.home',
    'uses'   => 'Painel\HomeController@index'
]);

Route::get('painel/login', [
    'as'   => 'painel.login',
    'uses' => 'Painel\HomeController@login'
]);
Route::post('painel/login', [
    'as'   => 'painel.auth',
    'uses' => 'Painel\HomeController@attempt'
]);
Route::get('painel/logout', [
    'as'   => 'painel.logout',
    'uses' => 'Painel\HomeController@logout'
]);

Route::group(['prefix' => 'painel', 'before' => 'auth'], function() {
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    Route::resource('abrangencia/unidades', 'Painel\UnidadesController');
    Route::resource('abrangencia', 'Painel\AbrangenciaController');

    Route::resource('cases', 'Painel\CasesController');

    Route::resource('curriculos', 'Painel\CurriculosController');

    Route::resource('imprensa/categorias', 'Painel\ImprensaCategoriasController');
    Route::resource('imprensa', 'Painel\ImprensaController');
    Route::resource('imprensa.imagens', 'Painel\ImprensaImagensController');

    Route::resource('contato/recebidos', 'Painel\ContatosRecebidosController');
    Route::resource('contato', 'Painel\ContatoController');

    Route::resource('usuarios', 'Painel\UsuariosController');

    Route::post('ajax/order', 'Painel\AjaxController@order');
});
