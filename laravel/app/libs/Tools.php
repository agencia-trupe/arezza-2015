<?php

class Tools
{

    public static function cropText($text = null, $length = null)
    {
        if (!$text || !$length) return false;

        $length = abs((int)$length);

        if(strlen($text) > $length) {
            $text = preg_replace("/^(.{1,$length})(\s.*|$)/s", '\\1...', strip_tags($text));
        }

        return $text;
    }

}
