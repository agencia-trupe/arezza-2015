<?php

class Unidade extends Eloquent
{

    protected $table = 'unidades';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC');
    }

}
