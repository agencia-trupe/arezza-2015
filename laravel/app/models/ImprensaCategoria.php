<?php

class ImprensaCategoria extends Eloquent
{

    protected $table = 'imprensa_categorias';

    protected $hidden = [];

    protected $guarded = ['id'];

}
