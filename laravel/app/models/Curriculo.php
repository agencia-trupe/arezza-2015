<?php

class Curriculo extends Eloquent
{

    protected $table = 'curriculos';

    protected $hidden = [];

    protected $guarded = ['id'];

    public function scopeDownload($query, $key)
    {
        return $query->where('download_key', $key);
    }
}
