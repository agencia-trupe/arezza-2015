<h3>MERCHANDISING E MARKETING</h3>
<p>A AREZZA oferece soluções criativas em marketing promocional, para atingir o cliente final no momento certo, no momento da compra. O treinamento das equipes garante a reprodução da cultura e da linguagem do cliente para que a mensagem institucional seja sempre a mesma, na empresa ou nas suas ações de marketing e merchandising. Isso garante transparência na comunicação que se reflete na venda sólida e reforço da imagem.</p>
<table>
    <thead>
        <th colspan="2">A AREZZA foca seu trabalho em alguns pontos principais:</th>
    </thead>
    <tr>
        <td>todo projeto é customizado e personalizado para cada cliente</td>
        <td>análise das condições do mercado local</td>
    </tr>
    <tr>
        <td>seleção de perfil adequado de pessoal</td>
        <td>os melhores profissionais do mercado, de gerentes a coordenadores e analistas</td>
    </tr>
    <tr>
        <td>planejamento estratégico dos canais de venda</td>
        <td>execução e finalização da venda</td>
    </tr>
</table>
<p>A AREZZA oferece soluções comprovadas para o ponto de contato com os talentos e serviços que você precisa para programas de marketing e merchandising de sucesso.</p>
<p>A AREZZA conta também com ambientes exclusivos para o segmento de Marketing e Merchandising em São Paulo e no Rio de Janeiro.</p>
