<h3>MARKETING PROMOCIONAL (TRADE MARKETING)</h3>
<p>Há mais de 10 anos a AREZZA fornece serviços especializados em trade marketing no Brasil. Atuando desde o planejamento, possui comprovada experiência na coordenação centralizada para eventos e programas em todo o território nacional, com presença internacional disponível quando preciso.</p>
<img src="{{ asset('assets/img/layout/mao-de-obra/marketing-promocao/merchandising.jpg') }}" alt="">
