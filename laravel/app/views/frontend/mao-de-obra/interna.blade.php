@section('content')

    <main class="mao-de-obra-main mao-de-obra-interna">
        <div class="center">
            <div>
            @foreach($rotas as $menu => $subs)
            <div class="col @if($slug == $menu) active @endif">
                <div class="imagem {{ $menu }}"></div>
                <div class="links">
                    <a href="{{ route('mao-de-obra', $menu) }}" class="botao-menu @if($slug == $menu) active @endif">{{ $menu_nome[$menu] }}<span></span></a>
                </div>
            </div>
            @endforeach
            </div>
            <div class="conteudo">
                <div class="subs">
                    @foreach($rotas[$slug] as $sub_slug => $sub)
                    <a href="{{ route('mao-de-obra', [$slug, $sub_slug]) }}" @if($sub_slug == $interna) class="active" @endif>{{ $sub }}</a>
                    @endforeach
                </div>
                <div class="include">
                    @include('frontend.mao-de-obra.'.$slug.'.'.$interna)
                </div>
            </div>
            <a href="{{ route('mao-de-obra') }}" class="voltar">voltar</a>
        </div>
        <div class="chamada-inferior">
            <div class="center">
                <span class="titulo">NÃO CONTRATE MÃO DE OBRA SEM ANTES CONSULTAR A AREZZA.</span>
                <span>Você pode se surpreender com nossas taxas de administração competitivas.</span>
            </div>
        </div>
    </main>

@stop
