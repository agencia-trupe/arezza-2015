<h3>HEADCOUNT</h3>
<p>A divisão de Headcount foi criada para minimizar o trabalho de nossos clientes na busca de profissionais temporários e terceiros. Contamos com uma equipe altamente qualificada e estruturada para a gestão deste trabalho.</p>
<img src="{{ asset('assets/img/layout/mao-de-obra/mao-de-obra/headcount.png') }}" alt="">
