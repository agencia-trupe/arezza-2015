<h3>TRABALHO TEMPORÁRIO</h3>
<p>O trabalho temporário tem como  objetivo suprir uma necessidade imediata e extraordinária da empresa cliente. Dinamiza a produtividade através da continuidade dos serviços, sempre que ocorrem imprevistos no seu quadro permanente, por aumento repentino do volume de trabalho, ausências em virtude de férias, doenças, acidentes de trabalho, licença-maternidade, treinamentos e outros.</p>
<h3>Compare custos</h3>
<img src="{{ asset('assets/img/layout/mao-de-obra/mao-de-obra/temporario.png') }}" alt="">
<p>Além da seleção de profissionais temporários, administramos a contratação deles, de acordo com a Legislação Trabalhista e Previdenciária vigente (Lei 6019/74).</p>
<p>O trabalho ou contrato temporário permite que o trabalhador escolha quando e onde quer trabalhar. Os contratos podem ser curtos – a partir de um dia de duração – ou longos, com até 270 dias de duração. O trabalho temporário é aplicado a qualquer cargo, desde o auxiliar até o gerente, diretor. Além disso, é uma forma econômica de contratar, seguindo os preceitos da lei 6.019.</p>
<div class="temp-left">
    <h3>Vantagens da Contratação Temporária:</h3>
    <ul>
        <li>Redução de custos com encargos trabalhistas</li>
        <li>Eliminação de gastos com recrutamento, seleção e treinamento de pessoal</li>
        <li>Reposição de trabalhador não adaptado, sem custo adicional</li>
        <li>Ausência de vínculo empregatício com o trabalhador</li>
        <li>Controle rigoroso do recolhimento de encargos sociais e fiscais</li>
        <li>Eliminação de ociosidade e aumento de produtividade</li>
        <li>Diminuição do passivo trabalhista</li>
        <li>Foco na atividade fim</li>
        <li>Redução de investimentos em serviço de apoio</li>
    </ul>
</div>
<div class="temp-right">
    <div class="title">
        <span class="first">Redução</span>
        <span class="last">de Taxa</span>
    </div>
    <p>Para contratações em grande escala no setor de Construção Civil a taxa de administração da AREZZA pode ser reduzida.</p>
    <a href="{{ route('contato') }}">Consulte-nos</a>
</div>
