<h3>ADMINISTRAÇÃO PLENA CLT</h3>
<p>Assumimos a administração das rotinas do Capital Humano de acordo com a Legislação Trabalhista e Previdenciária vigente. É a solução indicada para períodos superiores a 180 dias, podendo a contratação ser feita por prazo determinado ou indeterminado.</p>
<p>Na terceirização de mão de obra a AREZZA seleciona os melhores candidatos e assegura que a mão de obra contratada atue em atividades de suporte ao core business da Empresa. Uma das vantagens desta forma de contratação é que a AREZZA assegura todos os cálculos referentes à folha de pagamento, benefícios e impostos pagos ao Governo, poupando o trabalho de recrutamento e contratação, processamento da folha e cumprimento de obrigações fiscais, trabalhistas e as obrigações acessórias.</p>
<h3>Contratação por Prazo Determinado – Vantagens e regras:</h3>
<div class="col">
    <img src="{{ asset('assets/img/layout/mao-de-obra/mao-de-obra/adm-1.png') }}" alt="">
    <p>Datas de início e término pré-fixadas</p>
</div>
<div class="col">
    <img src="{{ asset('assets/img/layout/mao-de-obra/mao-de-obra/adm-2.png') }}" alt="">
    <p>Regras da CLT</p>
</div>
<div class="col">
    <img src="{{ asset('assets/img/layout/mao-de-obra/mao-de-obra/adm-3.png') }}" alt="">
    <p>Prorrogação permitida uma única vez, desde que não ultrapasse dois anos. Caso contrário, se tornará indeterminado automaticamente.</p>
</div>
<div class="col">
    <img src="{{ asset('assets/img/layout/mao-de-obra/mao-de-obra/adm-4.png') }}" alt="">
    <p>Exclui, obrigatoriamente, de Aviso Prévio e indenização de 40% do FGTS na rescisão do contrato de trabalho quando a demissão, impreterivelmente, ocorrer na data estabelecida.</p>
</div>
