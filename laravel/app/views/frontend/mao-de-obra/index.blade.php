@section('content')

    <main class="mao-de-obra-main">
        <div class="center">
            @foreach($rotas as $menu => $subs)
            <div class="col">
                <div class="imagem {{ $menu }}"></div>
                <div class="links">
                    <a href="{{ route('mao-de-obra', $menu) }}" class="botao-menu">{{ $menu_nome[$menu] }}<span></span></a>
                    <div class="subs">
                        @foreach($subs as $slug => $sub)
                        <a href="{{ route('mao-de-obra', [$menu, $slug]) }}">{{ $sub }}</a>
                        @endforeach
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </main>

@stop
