<h3>CONTROLE DE ACESSO, MANUTENÇÃO, LIMPEZA, JARDINAGEM</h3>
<p>Para este segmento a AREZZA conta com colaboradores amplamente capacitados e grande experiência de mercado adquirida no setor privado e no setor público, onde atende os maiores órgãos nacionais. A atuação focada no resultado do cliente elabora projetos específicos baseados em métodos e processos comprovados, com materiais e equipamentos inovadores.</p>
<img src="{{ asset('assets/img/layout/mao-de-obra/facilities/controle.jpg') }}" alt="">
<h5>NOSSOS SERVIÇOS DE LIMPEZA E CONSERVAÇÃO ATENDEM TODOS OS SEGMENTOS:</h5>
<div class="hexagono industrias">
    <h6>INDÚSTRIAS</h6>
    <p>Serviços de higienização em instalações e linhas de produção, vestiários, pátios, estacionamentos, área fabril e escritórios.</p>
</div>
<div class="hexagono saude">
    <h6>SAÚDE</h6>
    <p>Serviços de higienização em áreas comuns, sanitários, ruas e pátios.</p>
</div>
<div class="hexagono shoppings">
    <h6>SHOPPINGS</h6>
    <p>Serviços de higienização de sanitários, mall, praça de alimentação, estacionamento, fraldário, docas, galerias técnicas e tratamento de pisos.</p>
</div>
<div class="hexagono outros">
    <h6>OUTROS</h6>
    <p>Academias, hipermercados, aeroportos, instituições de ensino e demais segmentos.</p>
</div>
