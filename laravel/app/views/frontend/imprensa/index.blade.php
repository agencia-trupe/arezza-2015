@section('content')

    <main class="imprensa">
        <div class="center">
            <aside>
                <span>Categorias</span>
                <a href="{{ route('imprensa') }}" @if(!$filtro) class="active" @endif>Todas</a>
                @foreach($categorias as $categoria)
                <a href="{{ route('imprensa.filtro', $categoria->id) }}" @if($filtro && $categoria->id == $filtro->id) class="active" @endif>{{ $categoria->categoria }}</a>
                @endforeach
            </aside>

            <div class="noticias">
                @if(count($noticias))
                @foreach($noticias as $noticia)
                <a href="{{ route('imprensa', $noticia->slug) }}" class="noticia">
                    <div class="info">
                        <p class="data">
                            {{ $noticia->data }} <span>|</span> Autor: {{ $noticia->autor }}
                        </p>
                        <h3>{{ $noticia->titulo }}</h3>
                        <p class="olho">{{ $noticia->olho }}</p>
                        <div class="texto">{{ Tools::cropText(strip_tags($noticia->texto), 300) }}</div>
                    </div>

                    @if($noticia->video)
                    <div class="capa capa-video">
                        <img src="http://img.youtube.com/vi/{{ $noticia->video }}/mqdefault.jpg" alt="">
                    </div>
                    @elseif($noticia->capa)
                    <div class="capa">
                        <img src="{{ asset('assets/img/imprensa/capa/'.$noticia->capa) }}" alt="">
                    </div>
                    @endif

                    <div class="link"><span>Leia mais</span></div>
                </a>
                @endforeach
                {{ $noticias->links('pagination::simple') }}
                @else
                <p class="none">Nenhuma notícia cadastrada nessa categoria</p>
                @endif
            </div>
        </div>
    </main>

@stop
