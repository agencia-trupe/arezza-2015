@section('content')

    <main class="imprensa-noticia">
        <div class="center">
            <div class="noticia">
                <p class="data">
                    {{ $noticia->data }} <span>|</span> Autor: {{ $noticia->autor }}
                </p>
                <h3>{{ $noticia->titulo }}</h3>
                <p class="olho">{{ $noticia->olho }}</p>

                @if($noticia->video)
                <div class="video">
                    <div class="embed-video">
                        <iframe src="http://www.youtube.com/embed/{{ $noticia->video }}" frameborder="0"/></iframe>
                    </div>
                </div>
                @elseif($noticia->imagens && count($noticia->imagens) > 1)
                <div class="imagem" id="cycle">
                    @foreach($noticia->imagens as $img)
                    <img src="{{ asset('assets/img/imprensa/'.$img->imagem) }}" alt="">
                    @endforeach
                    <a href="#" id="prev">Anterior</a>
                    <a href="#" id="next">Próximo</a>
                </div>
                @elseif(count($noticia->imagens) == 1)
                <div class="imagem">
                    <img src="{{ asset('assets/img/imprensa/'.$noticia->imagens[0]->imagem) }}" alt="">
                </div>
                @endif

                <div class="texto">
                    {{ $noticia->texto }}
                </div>

                @if($noticia->pdf)
                    <a href="{{ url('assets/imprensa_pdf/'.$noticia->pdf) }}" class="pdf" target="_blank">Baixar PDF</a>
                @endif

                <a href="{{ route('imprensa') }}" class="voltar">Voltar</a>
            </div>

            <aside>
                @foreach($sidebar as $noticia)
                <a href="{{ route('imprensa', $noticia->slug) }}">
                    <p>{{ $noticia->data }}</p>
                    <p>Autor: {{ $noticia->autor }}</p>
                    <h4>{{ $noticia->titulo }}</h4>
                </a>
                @endforeach
            </aside>
        </div>
    </main>

@stop
