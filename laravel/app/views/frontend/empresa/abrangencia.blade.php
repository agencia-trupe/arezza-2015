<div class="matrizes">
    <span class="titulo">MATRIZ</span>
    {{ $abrangencia->matriz }}

    <span class="titulo">MATRIZES REGIONAIS</span>
    {{ $abrangencia->matrizes_regionais }}
    <img src="{{ asset('assets/img/layout/empresa/abrangencia.png') }}" alt="">
</div>

<div class="unidades">
    <span class="titulo">UNIDADES GENERALISTAS</span>

    <table>
    @foreach($unidades as $unidade)
        <tr>
            <td class="cidade">{{ $unidade->cidade }}</td>
            <td>{{ $unidade->endereco }}</td>
            <td class="telefone">{{ $unidade->telefone }}</td>
        </tr>
    @endforeach
    </table>

    <p><strong>IMPORTANTE</strong>: Nas unidades generalistas existe um profissional da Arezza totalmente dedicado ao Cliente no momento da demanda.</p>
</div>
