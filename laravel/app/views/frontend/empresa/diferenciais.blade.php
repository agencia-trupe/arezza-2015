<div class="col">
    <span class="titulo">Gestão corporativa</span>
    <img src="{{ asset('assets/img/layout/empresa/diferenciais-1.png') }}" alt="">
    <p>A gestão corporativa é a alma da Arezza. A comunidade de colaboradoes é fundamental para a empresa, garantindo que o fluxo de informações seja comunicado a todos os participantes, seja da diretoria ou do corpo de profissionais.</p>
</div>
<div class="col">
    <span class="titulo">nos responsabilizamos por reclamações trabalhistas</span>
    <img src="{{ asset('assets/img/layout/empresa/diferenciais-2.png') }}" alt="">
    <p>A AREZZA assume toda responsabilidade por reclamações trabalhistas. Essa é uma premissa da empresa, reconhecida por nossos clientes, que faz parte de nossos valores e nosso foco em atender sempre de maneira idônea.</p>
</div>
<div class="col">
    <span class="titulo">Princípios de equilíbrio financeiro</span>
    <img src="{{ asset('assets/img/layout/empresa/diferenciais-3.png') }}" alt="">
    <p>A AREZZA possui equipe financeira e investimentos necessários para atender à operação sem falhas de capital que possam impactar a operação, garantindo assim o cumprimento de todos os compromisos de pagamento com os trabalhadores em 100%.</p>
</div>
<div class="col">
    <span class="titulo">tecnologia</span>
    <img src="{{ asset('assets/img/layout/empresa/diferenciais-4.png') }}" alt="">
    <p>Desde 2005 a GINFOR elaborou uma plataforma exclusiva para a AREZZA. Entre outras coisas, nosso cliente pode aprovar “in locco” a folha de pagamento. Assim, ao receber a fatura não terá dificuldade para aprovação.</p>
</div>
