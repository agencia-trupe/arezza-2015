@section('content')

    <main class="empresa">
        <div class="chamada">
            <div class="center">
                <img src="{{ asset('assets/img/layout/empresa/chamada.png') }}" alt="">
                <div class="texto">
                    <h2>Não contrate mão de obra sem consultar a Arezza.</h2>
                    <p>Fundada em 1997, a AREZZA é uma companhia 100% brasileira. Criada e dirigida até hoje por um especialista em recursos humanos. Soluções para mão de obra são a especialidade da empresa. Soluções estas especialmente voltadas para resultados imediatos, como os temporários de acordo com a lei 6.019 e os contratos por prazo indeterminado. Exatamente aquilo que os clientes mais necessitam.</p>
                    <p>Nesses <strong>mais de 18 anos</strong> em atividade ininterrupta, o fundador sempre teve extrema preocupação em criar ferramentas que pudessem passar o máximo de segurança aos clientes. Entendemos que isso transformou uma pequena consultoria em um negócio que contrata mais de <strong>45.000 pessoas</strong> ao ano.</p>
                </div>
            </div>
        </div>

        <div class="center">
            <nav class="submenu">
                <a href="{{ route('empresa', 'diferenciais') }}" @if($slug == 'diferenciais') class="active" @endif>Diferenciais<span></span></a>
                <a href="{{ route('empresa', 'infraestrutura') }}" @if($slug == 'infraestrutura') class="active" @endif>Infraestrutura<span></span></a>
                <a href="{{ route('empresa', 'abrangencia') }}" @if($slug == 'abrangencia') class="active" @endif>Abrangência<span></span></a>
                <a href="{{ route('empresa', 'franquias') }}" @if($slug == 'franquias') class="active" @endif>Franquias<span></span></a>
            </nav>

            <div class="conteudo {{ $slug }}">
                @include('frontend.empresa.'.$slug)
            </div>
        </div>

        <div class="barra-inferior">
            <div class="center">
                <div class="slides">
                    <nav>
                        <a href="#" data-slide="missao" class="active">Missão</a>
                        <a href="#" data-slide="etica">Ética</a>
                        <a href="#" data-slide="visao">Visão</a>
                    </nav>

                    <div class="slide inicial" data-slide="missao">
                        <p>Ser uma empresa inovadora em servir o capital humano de modo que os clientes e parceiros se sintam protegidos e respeitados, associando o alto espírito de equipe com a necessidade em servir e atender com qualidade e eficácia.</p>
                        <img src="{{ asset('assets/img/layout/empresa/missao.png') }}" alt="">
                    </div>
                    <div class="slide" data-slide="etica">
                        <p>O Código de Ética da AREZZA não se restringe somente aos princípios que devem orientar o trabalho e as relações na empresa, mas também a conduta ética que cada profissional deve adotar para a elevação da qualidade dos produtos e serviços, bem como da nossa participação na sociedade.</p>
                        <img src="{{ asset('assets/img/layout/empresa/etica.png') }}" alt="">
                    </div>
                    <div class="slide" data-slide="visao">
                        <p>Ser referência em recrutamento e seleção e satisfazer as necessidades do mercado, mantendo a competitividade de preço e eficácia no atendimento.</p>
                        <img src="{{ asset('assets/img/layout/empresa/visao.png') }}" alt="">
                    </div>
                </div>

                <div class="premios">
                    <span>Prêmios</span>
                    <img src="{{ asset('assets/img/layout/empresa/premios.png') }}" alt="">
                </div>
            </div>
        </div>
    </main>

@stop
