<div class="col">
    <span class="titulo">mais de 18 anos no mercado</span>
    <img src="{{ asset('assets/img/layout/empresa/franquias-1.png') }}" alt="">
    <p>É uma marca de mais de 18 anos, <strong>reconhecida por prêmios nacionais e internacionais</strong>, em 2015 eleita melhor consultoria de recursos humanos pela revista Gestão e RH e uma das 10 melhores empresas de serviços do Brasil.</p>
</div>
<div class="col">
    <span class="titulo">você não tem taxa nenhuma</span>
    <img src="{{ asset('assets/img/layout/empresa/franquias-2.png') }}" alt="">
    <p>Você não tem taxa nenhuma para utilizar a marca Arezza. <strong>Fazemos todo suporte financeiro para você captar clientes</strong>, com isso bancar toda folha, benefícios, rescisões e impostos.</p>
</div>
<div class="col">
    <span class="titulo">equipe de apoio experiente</span>
    <img src="{{ asset('assets/img/layout/empresa/franquias-3.png') }}" alt="">
    <p>Uma equipe de advogados e assistentes dará total apoio ao seu negócio. Analisarão contratos, riscos da contratação, sindicatos, órgãos locais, entre outros. <strong>Todo o setor financeiro estará á sua disposição</strong>. E tudo isso sem custo para o franqueado ou autorizado.</p>
</div>
<div class="col">
    <span class="titulo">diversas regiões de atuação</span>
    <img src="{{ asset('assets/img/layout/empresa/franquias-4.png') }}" alt="">
    <p>Você vai atuar nas regiões autorizadas, cuidando da <strong>prospecção e geração de negócios</strong> usando sua influência e conhecimentos comerciais,  tendo toda nossa estrutura a seu dispor, como o departamento jurídico, TI, setor de pessoal, setor financeiro, entre outros.</p>
</div>
