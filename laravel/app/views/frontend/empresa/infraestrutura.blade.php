<div class="texto">
    <p>A AREZZA possui sede própria no Bairro Campo Belo, próximo ao aeroporto de Congonhas. Em nossa sede fica todo o Staff Administrativo. A estrutura é composta também de células de clientes. Esse formato diminui muitos problemas administrativos e operacionais. Conforme a demanda do cliente, dedicamos profissionais focados em suas necessidades. Uma das maiores células é dedicada ao maior fabricante de bebidas do mundo e outras duas ao varejo e construção civil.</p>
    <p>Outro foco importante da empresa são os sistemas de informática. Desde 2005 a GINFOR elaborou uma plataforma exclusiva para a AREZZA. Entre outras coisas, nosso cliente pode aprovar “in locco” a folha de pagamento. Assim, ao receber a fatura não terá dificuldade para aprovação.</p>
</div>
<img src="{{ asset('assets/img/layout/empresa/infraestrutura.jpg') }}" alt="">
