@section('content')

    <main class="vagas">
        <div class="chamada">
            <div class="center">
                <h2>Envie seu Currículo</h2>
            </div>
        </div>

        <div class="center">
            <form action="" id="form-curriculo" enctype="multipart/form-data">
                <input type="text" name="nome" id="nome" placeholder="NOME" required>
                <input type="email" name="email" id="email" placeholder="E-MAIL" required>
                <input type="text" name="telefone" id="telefone" placeholder="TELEFONE">
                <input type="text" name="celular" id="celular" placeholder="CELULAR">
                <textarea name="apresentacao" id="apresentacao" placeholder="TEXTO DE APRESENTAÇÃO" required></textarea>
                <label id="curriculo">
                    <div class="botao">Anexar currículo</div>
                    <span>Nenhum arquivo selecionado</span>
                    <input type="file" name="curriculo">
                </label>
                <input type="submit" value="ENVIAR">

                <div id="form-resposta"></div>
            </form>

            <div class="info">
                <p>Se preferir encaminhe seu currículo e resumo por e-mail: <a href="mailto:selecao@arezza.com.br">selecao@arezza.com.br</a></p>
                <a href="{{ $contato->facebook }}" class="facebook" target="_blank">
                    Ou acompanhe a AREZZA nas redes sociais: <span>facebook.com/arezza.recursoshumanos</span>
                    <span class="icone"></span>
                </a>
                <p>Você também pode deixar seu currículo em uma de nossas unidades generalistas.</p>
                <a href="{{ route('empresa', 'abrangencia') }}" class="enderecos">Ver endereços</a>
                <p>Boa sorte. O cadastro em nosso banco de currículos é sempre gratuito. Bem-vindo à AREZZA.</p>
            </div>
        </div>
    </main>

@stop
