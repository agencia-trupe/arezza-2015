@section('content')

    <main class="home">
        <div class="center">
            <a href="{{ route('mao-de-obra', 'mao-de-obra') }}" class="mao-de-obra">
                <h2>Soluções em <br>Mão de Obra</h2>
            </a>
            <a href="{{ route('mao-de-obra', 'facilities') }}" class="facilities">
                <h2>Facilities</h2>
            </a>
            <a href="{{ route('mao-de-obra', 'marketing-e-promocao') }}" class="marketing">
                <h2>Marketing <br>& Promoção</h2>
            </a>
        </div>
    </main>

@stop
