@section('content')

    <main class="contato">
        <div class="chamada">
            <div class="center">
                <h2><span class="telefone">{{ $contato->telefone }}</span> das 8h às 18h de 2ª a 6ª feira</h2>
            </div>
        </div>

        <div class="center contato-form">
            <form action="" id="form-contato" enctype="multipart/form-data">
                <span class="titulo">MENSAGEM</span>
                <input type="text" name="nome" id="nome" placeholder="NOME" required>
                <input type="email" name="email" id="email" placeholder="E-MAIL" required>
                <input type="text" name="telefone" id="telefone" placeholder="TELEFONE">
                <input type="text" name="empresa" id="empresa" placeholder="EMPRESA">
                <textarea name="mensagem" id="mensagem" placeholder="MENSAGEM" required></textarea>
                <input type="submit" value="ENVIAR">

                <div id="form-resposta"></div>
            </form>

            <div class="imagem"></div>

            <div class="assessoria">
                <p class="titulo">Assessoria de imprensa</p>
                <h3>{{ $contato->assessoria_titulo }}</h3>
                <div class="texto">
                    {{ $contato->assessoria_info }}
                </div>
            </div>

        </div>

        <div class="contato-success">
            <img src="{{ asset('assets/img/layout/contato/foto.jpg') }}" alt="">
            <span>OBRIGADO!<br>Responderemos em breve.</span>
        </div>
    </main>

@stop
