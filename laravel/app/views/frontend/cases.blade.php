@section('content')

    <main class="cases">
        <div class="chamada">
            <div class="center">
                <img src="{{ asset('assets/img/layout/cases/chamada.png') }}" alt="">
                <div class="texto">
                    <h2>Conheça alguns dos nossos cases de sucesso.</h2>
                    <p>Acreditamos que essas informações possam ajudar você momento de tomar a decisão de contratar a AREZZA. Selecionamos os casos recentes que marcaram a nossa empresa.</p>
                </div>
            </div>
        </div>

        <div class="center">
            <nav class="submenu">
                @foreach($cases as $case)
                <a href="{{ route('cases', $case->id) }}" @if($case->id == $selecionado->id) class="active" @endif>{{ $case->titulo }}</a>
                @endforeach
            </nav>

            <div class="conteudo">
                @if($selecionado->imagem)
                <img src="{{ asset('assets/img/cases/'.$selecionado->imagem) }}" alt="">
                @endif

                <p class="titulo">{{ $selecionado->titulo }}</p>
                <p class="olho">{{ $selecionado->olho }}</p>
                {{ $selecionado->texto }}
            </div>
        </div>
    </main>

@stop
