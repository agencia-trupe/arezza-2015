    <header>
        <div class="center">
            <h1><a href="{{ route('home') }}">{{ Config::get('projeto.title') }}</a></h1>

            <nav id="desktop">
                <ul class="links">
                    <li><a href="{{ route('vagas') }}">Enviar Currículo</a></li>
                    <li><a href="#" target="_blank">Folha Fácil</a></li>
                </ul>

                <ul class="menu">
                    <li><a href="{{ route('empresa') }}" @if(str_is('empresa*', Route::currentRouteName())) class='active' @endif>Empresa</a></li>
                    <li><a href="{{ route('mao-de-obra') }}" @if(str_is('mao-de-obra*', Route::currentRouteName())) class='active' @endif>Mão de Obra</a></li>
                    <li><a href="{{ route('cases') }}" @if(str_is('cases*', Route::currentRouteName())) class='active' @endif>Cases</a></li>
                    <li><a href="{{ route('vagas') }}" @if(str_is('vagas*', Route::currentRouteName())) class='active' @endif>Vagas</a></li>
                    <li><a href="{{ route('imprensa') }}" @if(str_is('imprensa*', Route::currentRouteName())) class='active' @endif>Imprensa</a></li>
                    <li><a href="{{ route('contato') }}" @if(str_is('contato*', Route::currentRouteName())) class='active' @endif>Contato</a></li>
                </ul>
            </nav>

            <div class="telefone">{{ $contato->telefone }}</div>
        </div>

        <nav id="mobile">
            <ul class="menu">
                <li><a href="{{ route('empresa') }}" @if(str_is('empresa*', Route::currentRouteName())) class='active' @endif>Empresa</a></li>
                <li><a href="{{ route('mao-de-obra') }}" @if(str_is('mao-de-obra*', Route::currentRouteName())) class='active' @endif>Mão de Obra</a></li>
                <li><a href="{{ route('cases') }}" @if(str_is('cases*', Route::currentRouteName())) class='active' @endif>Cases</a></li>
                <li><a href="{{ route('vagas') }}" @if(str_is('vagas*', Route::currentRouteName())) class='active' @endif>Vagas</a></li>
                <li><a href="{{ route('imprensa') }}" @if(str_is('imprensa*', Route::currentRouteName())) class='active' @endif>Imprensa</a></li>
                <li><a href="{{ route('contato') }}" @if(str_is('contato*', Route::currentRouteName())) class='active' @endif>Contato</a></li>
            </ul>

            <ul class="links">
                <li><a href="{{ route('vagas') }}">Enviar Currículo</a></li>
                <li><a href="#" target="_blank">Folha Fácil</a></li>
            </ul>
        </nav>
        <button id="mobile-toggle" type="button" role="button">
            <span class="lines"></span>
        </button>
    </header>
