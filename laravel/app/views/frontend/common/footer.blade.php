    <footer @if(str_is('home*', Route::currentRouteName())) class='home' @endif>
        <div class="center">
            <div class="col left">
                <span class="title">Empresa</span>
                <a href="{{ route('empresa', 'diferenciais') }}">Diferenciais</a>
                <a href="{{ route('empresa', 'infraestrutura') }}">Infraestrutura</a>
                <a href="{{ route('empresa', 'abrangencia') }}">Abrangência</a>
                <a href="{{ route('empresa', 'franquias') }}">Franquias</a>
            </div>

            <div class="col left">
                <span class="title">Mão de Obra</span>
                <a href="{{ route('mao-de-obra', 'mao-de-obra') }}">Mão de Obra</a>
                <a href="{{ route('mao-de-obra', 'facilities') }}">Facilities</a>
                <a href="{{ route('mao-de-obra', 'marketing-e-promocao') }}">Marketing & Promoção</a>
            </div>

            <div class="col left">
                <span class="title">Vagas</span>
                <a href="{{ route('vagas') }}">Envio de Currículo</a>
            </div>

            <div class="col right">
                <span class="title">Contato</span>
                <p>{{ $contato->telefone }} <span>8h - 18h</span></p>
                <a href="{{ route('contato') }}">Enviar mensagem</a>
                <a href="{{ route('contato') }}">Assessoria de imprensa</a>
                <ul class="social">
                    @if($contato->facebook)<li><a href="{{ $contato->facebook }}" class="facebook" target="_blank">facebook</a></li>@endif
                    @if($contato->twitter)<li><a href="{{ $contato->twitter }}" class="twitter" target="_blank">twitter</a></li>@endif
                    @if($contato->youtube)<li><a href="{{ $contato->youtube }}" class="youtube" target="_blank">youtube</a></li>@endif
                </ul>
            </div>
        </div>
    </footer>
    <div class="copyright">
        <div class="center">
            <p class="left">© {{ date('Y') }} {{ Config::get('projeto.name') }} - Todos os direitos reservados.</p>
            <p class="right">
                <a href="http://www.trupe.net" target="blank">Criação de sites</a>:
                <a href="http://www.trupe.net" target="blank" class="trupe">Trupe Agência Criativa</a>
            </p>
        </div>
    </div>
