<!DOCTYPE html>
<html>
<head>
    <title>[CURRÍCULO] {{ Config::get('projeto.titulo') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Celular:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $celular }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Texto de Apresentação:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $apresentacao }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Currículo:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'><a href="{{ route('download', $download_key) }}">Fazer Download</a></span>
</body>
</html>
