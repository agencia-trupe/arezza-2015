@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('matriz', 'Matriz') }}
    {{ Form::textarea('matriz', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) }}
</div>

<div class="form-group">
    {{ Form::label('matrizes_regionais', 'Matrizes Regionais') }}
    {{ Form::textarea('matrizes_regionais', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}
