@section('content')

    <legend>
        <h2><small>Abrangência /</small> Matrizes</h2>
    </legend>

    {{ Form::model($matrizes, [
        'route' => ['painel.abrangencia.update', $matrizes->id],
        'method' => 'patch'])
    }}

        @include('painel.abrangencia.matriz._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
