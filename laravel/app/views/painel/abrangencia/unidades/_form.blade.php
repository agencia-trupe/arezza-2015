@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('cidade', 'Cidade') }}
    {{ Form::text('cidade', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('endereco', 'Endereço') }}
    {{ Form::text('endereco', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('telefone', 'Telefone') }}
    {{ Form::text('telefone', null, ['class' => 'form-control']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.abrangencia.unidades.index') }}" class="btn btn-default btn-voltar">Voltar</a>
