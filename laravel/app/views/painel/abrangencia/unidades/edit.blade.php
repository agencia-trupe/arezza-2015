@section('content')

    <legend>
        <h2><small>Abrangência /</small> Editar Unidade</h2>
    </legend>

    {{ Form::model($unidade, [
        'route' => ['painel.abrangencia.unidades.update', $unidade->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.abrangencia.unidades._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
