@section('content')

    <legend>
        <h2><small>Abrangência /</small> Adicionar Unidade</h2>
    </legend>

    {{ Form::open(['route' => 'painel.abrangencia.unidades.store']) }}

        @include('painel.abrangencia.unidades._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
