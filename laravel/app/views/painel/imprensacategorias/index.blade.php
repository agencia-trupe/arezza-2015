@section('content')

    @if($errors->any())
        @foreach($errors->all() as $error)
        <div class="alert alert-block alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('sucesso'))
       <div class="alert alert-block alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::get('sucesso') }}
        </div>
    @endif

    <a href="{{ route('painel.imprensa.index') }}" title="Voltar para Imprensa" class="btn btn-default">&larr; Voltar para Imprensa</a>

    <legend>
        <h2>
            <small>Imprensa /</small> Categorias
            <a href="{{ route('painel.imprensa.categorias.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Categoria</a>
        </h2>
    </legend>

    @if(count($categorias))
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Categoria</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($categorias as $categoria)

            <tr class="tr-row" id="id_{{ $categoria->id }}">
                <td>{{ $categoria->categoria }}</td>
                <td class="crud-actions">
                    {{ Form::open(array('route' => array('painel.imprensa.categorias.destroy', $categoria->id), 'method' => 'delete')) }}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.imprensa.categorias.edit', $categoria->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    @else
        <div class="alert alert-warning" role="alert">Nenhuma categoria cadastrada.</div>
    @endif

@stop
