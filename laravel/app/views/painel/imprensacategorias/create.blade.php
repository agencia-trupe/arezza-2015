@section('content')

    <legend>
        <h2><small>Imprensa /</small> Adicionar Categoria</h2>
    </legend>

    {{ Form::open(['route' => 'painel.imprensa.categorias.store']) }}

        @include('painel.imprensacategorias._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
