@section('content')

    <legend>
        <h2><small>Imprensa /</small> Editar Categoria</h2>
    </legend>

    {{ Form::model($categoria, [
        'route' => ['painel.imprensa.categorias.update', $categoria->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.imprensacategorias._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
