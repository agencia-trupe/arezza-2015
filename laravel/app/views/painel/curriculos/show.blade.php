@section('content')

    <legend>
        <h2>Currículo</h2>
    </legend>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $curriculo->nome }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">{{ $curriculo->email }}</div>
    </div>

    <div class="form-group">
        <label>Telefone</label>
        <div class="well">{{ $curriculo->telefone }}</div>
    </div>

    <div class="form-group">
        <label>Celular</label>
        <div class="well">{{ $curriculo->celular }}</div>
    </div>

    <div class="form-group">
        <label>Texto de apresentação</label>
        <div class="well">{{ $curriculo->apresentacao }}</div>
    </div>

    <a href="{{ route('download', $curriculo->download_key) }}" class="btn btn-primary"><span class="glyphicon glyphicon-download" style="margin-right:10px;"></span>Download Currículo</a>

    <a href="{{ route('painel.curriculos.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
