@section('content')

    @if($errors->any())
        @foreach($errors->all() as $error)
        <div class="alert alert-block alert-danger">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ $error }}
        </div>
        @endforeach
    @endif

    @if(Session::has('sucesso'))
       <div class="alert alert-block alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            {{ Session::get('sucesso') }}
        </div>
    @endif

    <legend>
        <h2>Currículos</h2>
    </legend>

    @if(count($curriculos))
    <table class="table table-striped table-bordered table-hover ">
        <thead>
            <tr>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Currículo</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($curriculos as $curriculo)

            <tr class="tr-row">
                <td>{{ $curriculo->nome }}</td>
                <td>{{ $curriculo->email }}</td>
                <td><a href="{{ route('download', $curriculo->download_key) }}" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-download" style="margin-right:10px;"></span>Download</a></td>
                <td class="crud-actions">
                    {{ Form::open(array('route' => array('painel.curriculos.destroy', $curriculo->id), 'method' => 'delete')) }}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.curriculos.show', $curriculo->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Ficha completa
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {{ Form::close() }}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    {{ $curriculos->links() }}
    @else
        <div class="alert alert-warning" role="alert">Nenhum currículo recebido.</div>
    @endif

@stop
