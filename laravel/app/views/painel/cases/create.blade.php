@section('content')

    <legend>
        <h2>Adicionar Case</h2>
    </legend>

    {{ Form::open(['route' => 'painel.cases.store', 'files' => true]) }}

        @include('painel.cases._form', ['submitText' => 'Inserir'])

    {{ Form::close() }}

@stop
