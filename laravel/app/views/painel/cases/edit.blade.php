@section('content')

    <legend>
        <h2>Editar Case</h2>
    </legend>

    {{ Form::model($case, [
        'route' => ['painel.cases.update', $case->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.cases._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
