@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('titulo', 'Título') }}
    {{ Form::text('titulo', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('olho', 'Olho') }}
    {{ Form::text('olho', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('texto', 'Texto') }}
    {{ Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cases']) }}
</div>

<div class="well form-group">
    {{ HTML::decode(Form::label('imagem', 'Imagem <small>(200x160px)</small>')) }}
@if($submitText == 'Alterar' && $case->imagem)
    <img src="{{ url('assets/img/cases/'.$case->imagem) }}" class="img-responsive" style="display:block; margin-bottom: 10px">
    <div class="alert alert-info">
        {{ Form::checkbox('remover_imagem', 'true', null, ['id' => 'remover_imagem']) }}
        {{ Form::label('remover_imagem', 'Selecione para remover a imagem') }}
    </div>
@endif
    {{ Form::file('imagem', ['class' => 'form-control']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.cases.index') }}" class="btn btn-default btn-voltar">Voltar</a>
