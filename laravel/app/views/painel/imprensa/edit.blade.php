@section('content')

    <legend>
        <h2>Editar Notícia</h2>
    </legend>

    {{ Form::model($imprensa, [
        'route' => ['painel.imprensa.update', $imprensa->id],
        'method' => 'patch', 'files' => true])
    }}

        @include('painel.imprensa._form', ['submitText' => 'Alterar'])

    {{ Form::close() }}

@stop
