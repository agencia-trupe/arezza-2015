@if($errors->any())
    @foreach($errors->all() as $error)
    <div class="alert alert-block alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ $error }}
    </div>
    @endforeach
@endif

@if(Session::has('sucesso'))
   <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('sucesso') }}
    </div>
@endif

<div class="form-group">
    {{ Form::label('data', 'Data') }}
    {{ Form::text('data', null, ['class' => 'form-control ', 'id' => 'datepicker']) }}
</div>

<div class="form-group">
    {{ Form::label('categoria_id', 'Categoria (opcional)') }}
    {{ Form::select('categoria_id', ['' => 'Selecione'] + $categorias, null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('autor', 'Autor') }}
    {{ Form::text('autor', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('titulo', 'Título') }}
    {{ Form::text('titulo', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('olho', 'Olho') }}
    {{ Form::text('olho', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ HTML::decode(Form::label('video', 'Código do vídeo no YouTube <small>(opcional)</small>')) }}
    {{ Form::text('video', null, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::label('texto', 'Texto') }}
    {{ Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'imprensa']) }}
</div>

<div class="well form-group">
    {{ HTML::decode(Form::label('capa', 'Imagem de capa <small>(opcional, 230x215px)</small>')) }}
@if($submitText == 'Alterar' && $imprensa->capa)
    <img src="{{ url('assets/img/imprensa/capa/'.$imprensa->capa) }}" class="img-responsive" style="display:block; margin-bottom: 10px">
    <div class="alert alert-info">
        {{ Form::checkbox('remover_capa', 'true', null, ['id' => 'remover_capa']) }}
        {{ Form::label('remover_capa', 'Selecione para remover a imagem de capa') }}
    </div>
@endif
    {{ Form::file('capa', ['class' => 'form-control']) }}
</div>

<div class="well form-group">
    {{ HTML::decode(Form::label('pdf', 'Arquivo PDF <small>(opcional)</small>')) }}
@if($submitText == 'Alterar' && $imprensa->pdf)
    <a href="{{ url('assets/imprensa_pdf/'.$imprensa->pdf) }}" style="display:block;margin:10px 0;">{{ $imprensa->pdf }}</a>
    <div class="alert alert-info">
        {{ Form::checkbox('remover_pdf', 'true', null, ['id' => 'remover_pdf']) }}
        {{ Form::label('remover_pdf', 'Selecione para remover o arquivo pdf') }}
    </div>
@endif
    {{ Form::file('pdf', ['class' => 'form-control']) }}
</div>

{{ Form::submit($submitText, ['class' => 'btn btn-success']) }}

<a href="{{ route('painel.imprensa.index') }}" class="btn btn-default btn-voltar">Voltar</a>
