<?php

namespace Painel;

use \Imprensa, \ImprensaCategoria, \Input, \Validator, \Redirect, \Session, \Str, \CropImage;

class ImprensaController extends BasePainelController {

    private $validation_rules = [
        'data'   => 'required',
        'autor'  => 'required',
        'titulo' => 'required',
        'olho'   => 'required',
        'texto'  => 'required',
        'capa'   => 'image',
        'pdf'    => 'mimes:pdf'
    ];

    private $image_config = [
        'width'  => 230,
        'height' => 215,
        'path'   => 'assets/img/imprensa/capa/'
    ];

    public function index()
    {
        $imprensa = Imprensa::ordenados()->paginate(10);

        return $this->view('painel.imprensa.index', compact('imprensa'));
    }

    public function create()
    {
        $categorias = ImprensaCategoria::lists('categoria', 'id');

        return $this->view('painel.imprensa.create', compact('categorias'));
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));
            $input['capa'] = CropImage::make('capa', $this->image_config);
            if (Input::hasFile('pdf')) {
                $pdf        = $input['pdf'];
                $uploadPath = public_path('assets/imprensa_pdf/');
                $fileName   = date('YmdHis').'_'.$pdf->getClientOriginalName();

                $pdf->move($uploadPath, $fileName);
                $input['pdf'] = $fileName;
            }

            Imprensa::create($input);
            Session::flash('sucesso', 'Notícia adicionada com sucesso.');

            return Redirect::route('painel.imprensa.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar notícia. Verifique se já existe outra notícia com o mesmo título.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $imprensa   = Imprensa::findOrFail($id);
        $categorias = ImprensaCategoria::lists('categoria', 'id');

        return $this->view('painel.imprensa.edit', compact('imprensa', 'categorias'));
    }

    public function update($id)
    {
        $imprensa = Imprensa::findOrFail($id);
        $input    = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['slug'] = Str::slug(Input::get('titulo'));

            if (Input::hasFile('capa')) {
                $input['capa'] = CropImage::make('capa', $this->image_config);
            } else {
                unset($input['capa']);
            }

            if (Input::hasFile('pdf')) {
                $pdf        = $input['pdf'];
                $uploadPath = public_path('assets/imprensa_pdf/');
                $fileName   = date('YmdHis').'_'.$pdf->getClientOriginalName();

                $pdf->move($uploadPath, $fileName);
                $input['pdf'] = $fileName;
            } else {
                unset($input['pdf']);
            }

            if (Input::has('remover_capa')) {
                $input['capa'] = null;
                unset($input['remover_capa']);
            }

            if (Input::has('remover_pdf')) {
                $input['pdf'] = null;
                unset($input['remover_pdf']);
            }

            $imprensa->update($input);
            Session::flash('sucesso', 'Notícia alterada com sucesso.');

            return Redirect::route('painel.imprensa.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar notícia. Verifique se já existe outra notícia com o mesmo título.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Imprensa::destroy($id);
            Session::flash('sucesso', 'Notícia removida com sucesso.');

            return Redirect::route('painel.imprensa.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover notícia.']);

        }
    }

}
