<?php

namespace Painel;

use \Abrangencia, \Input, \Validator, \Redirect, \Session;

class AbrangenciaController extends BasePainelController {

    private $validation_rules = [
        'matriz'             => 'required',
        'matrizes_regionais' => 'required'
    ];

    public function index()
    {
        $matrizes = Abrangencia::first();

        return $this->view('painel.abrangencia.matriz.index', compact('matrizes'));
    }

    public function update($id)
    {
        $matrizes = Abrangencia::findOrFail($id);
        $input    = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $matrizes->update($input);
            Session::flash('sucesso', 'Informações alteradas com sucesso.');

            return Redirect::route('painel.abrangencia.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar informações.'])
                ->withInput();

        }
    }

}
