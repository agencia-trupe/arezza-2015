<?php

namespace Painel;

use \Unidade, \Input, \Validator, \Redirect, \Session;

class UnidadesController extends BasePainelController {

    private $validation_rules = [
        'cidade'   => 'required',
        'endereco' => 'required',
        'telefone' => 'required'
    ];

    public function index()
    {
        $unidades = Unidade::ordenados()->get();

        return $this->view('painel.abrangencia.unidades.index', compact('unidades'));
    }

    public function create()
    {
        return $this->view('painel.abrangencia.unidades.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            Unidade::create($input);
            Session::flash('sucesso', 'Unidade adicionada com sucesso.');

            return Redirect::route('painel.abrangencia.unidades.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao adicionar unidade.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $unidade = Unidade::findOrFail($id);

        return $this->view('painel.abrangencia.unidades.edit', compact('unidade'));
    }

    public function update($id)
    {
        $unidade = Unidade::findOrFail($id);
        $input   = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $unidade->update($input);
            Session::flash('sucesso', 'Unidade alterada com sucesso.');

            return Redirect::route('painel.abrangencia.unidades.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar unidade.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Unidade::destroy($id);
            Session::flash('sucesso', 'Unidade removida com sucesso.');

            return Redirect::route('painel.abrangencia.unidades.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover unidade.']);

        }
    }

}
