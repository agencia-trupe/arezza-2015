<?php

namespace Painel;

use \Cases, \Input, \Validator, \Redirect, \Session, \CropImage;

class CasesController extends BasePainelController {

    private $validation_rules = [
        'titulo' => 'required',
        'olho'   => 'required',
        'texto'  => 'required',
        'imagem' => 'image'
    ];

    private $image_config = [
        'width'  => 200,
        'height' => 160,
        'path'   => 'assets/img/cases/'
    ];

    public function index()
    {
        $cases = Cases::ordenados()->get();

        return $this->view('painel.cases.index', compact('cases'));
    }

    public function create()
    {
        return $this->view('painel.cases.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $input['imagem'] = CropImage::make('imagem', $this->image_config);
            Cases::create($input);
            Session::flash('sucesso', 'Case adicionado com sucesso.');

            return Redirect::route('painel.cases.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao criar case.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $case = Cases::findOrFail($id);

        return $this->view('painel.cases.edit', compact('case'));
    }

    public function update($id)
    {
        $case = Cases::findOrFail($id);
        $input  = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            if (Input::hasFile('imagem')) {
                $input['imagem'] = CropImage::make('imagem', $this->image_config);
            } else {
                unset($input['imagem']);
            }

            if (Input::has('remover_imagem')) {
                $input['imagem'] = null;
                unset($input['remover_imagem']);
            }

            $case->update($input);
            Session::flash('sucesso', 'Case alterado com sucesso.');

            return Redirect::route('painel.cases.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar case.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            Cases::destroy($id);
            Session::flash('sucesso', 'Case removido com sucesso.');

            return Redirect::route('painel.cases.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover case.']);

        }
    }

}
