<?php

namespace Painel;

use \Curriculo, \Session, \Redirect;

class CurriculosController extends BasePainelController {

    public function index()
    {
        $curriculos = Curriculo::orderBy('id', 'DESC')->paginate(10);

        return $this->view('painel.curriculos.index', compact('curriculos'));
    }

    public function show($id)
    {
        $curriculo = Curriculo::findOrFail($id);

        return $this->view('painel.curriculos.show', compact('curriculo'));
    }

    public function destroy($id)
    {
        try {

            Curriculo::destroy($id);
            Session::flash('sucesso', 'Currículo removido com sucesso.');

            return Redirect::route('painel.curriculos.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover currículo.']);

        }
    }

}
