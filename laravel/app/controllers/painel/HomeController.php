<?php

namespace Painel;

use \View, \Input, \Auth, \Session, \Redirect;

class HomeController extends BasePainelController {

    public function index()
    {
        return $this->view('painel.home');
    }

    public function login()
    {
        return $this->view('painel.login');
    }

    public function attempt()
    {
        $authvars = array(
            'username' => Input::get('username'),
            'password' => Input::get('password')
        );

        $lembrar = (Input::get('lembrar') == 'true') ? true : false;

        if (Auth::attempt($authvars, $lembrar)) {
            return Redirect::route('painel.home');
        } else {
            Session::flash('login_errors', true);
            return Redirect::route('painel.login')->withInput(Input::except('password'));
        }
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::route('home');
    }

}
