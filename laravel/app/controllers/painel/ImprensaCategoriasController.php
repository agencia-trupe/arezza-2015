<?php

namespace Painel;

use \ImprensaCategoria, \Input, \Validator, \Redirect, \Session;

class ImprensaCategoriasController extends BasePainelController {

    private $validation_rules = [
        'categoria' => 'required',
    ];

    public function index()
    {
        $categorias = ImprensaCategoria::get();

        return $this->view('painel.imprensacategorias.index', compact('categorias'));
    }

    public function create()
    {
        return $this->view('painel.imprensacategorias.create');
    }

    public function store()
    {
        $input = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            ImprensaCategoria::create($input);
            Session::flash('sucesso', 'Categoria adicionada com sucesso.');

            return Redirect::route('painel.imprensa.categorias.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao adicionar categoria.'])
                ->withInput();

        }
    }

    public function edit($id)
    {
        $categoria = ImprensaCategoria::findOrFail($id);

        return $this->view('painel.imprensacategorias.edit', compact('categoria'));
    }

    public function update($id)
    {
        $categoria = ImprensaCategoria::findOrFail($id);
        $input     = Input::all();

        $validate = Validator::make($input, $this->validation_rules);

        if ($validate->fails()) {
            return Redirect::back()
                ->withErrors($validate)
                ->withInput();
        }

        try {

            $categoria->update($input);
            Session::flash('sucesso', 'Categoria alterada com sucesso.');

            return Redirect::route('painel.imprensa.categorias.index');

        } catch (\Exception $e) {

            return Redirect::back()
                ->withErrors(['Erro ao alterar categoria.'])
                ->withInput();

        }
    }

    public function destroy($id)
    {
        try {

            ImprensaCategoria::destroy($id);
            Session::flash('sucesso', 'Categoria removida com sucesso.');

            return Redirect::route('painel.imprensa.categorias.index');

        } catch (\Exception $e) {

            return Redirect::back()->withErrors(['Erro ao remover categoria.']);

        }
    }

}
