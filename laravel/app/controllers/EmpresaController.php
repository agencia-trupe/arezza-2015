<?php

use \Abrangencia, \Unidade;

class EmpresaController extends BaseController {

    public function index($slug = null)
    {
        if (!$slug) $slug = 'diferenciais';
        if (!View::exists('frontend.empresa.'.$slug)) App::abort('404');

        if ($slug == 'abrangencia') {
            $abrangencia = Abrangencia::first();
            $unidades    = Unidade::ordenados()->get();
            return $this->view('frontend.empresa._template', compact('slug', 'abrangencia', 'unidades'));
        }

        return $this->view('frontend.empresa._template', compact('slug'));
    }

}
