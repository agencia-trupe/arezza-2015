<?php

use \Curriculo;

class VagasController extends BaseController {

    public function index()
    {
        $this->view('frontend.vagas');
    }

    public function envio()
    {
        $contato = Contato::first();

        $nome         = Input::get('nome');
        $email        = Input::get('email');
        $telefone     = Input::get('telefone');
        $celular      = Input::get('celular');
        $apresentacao = Input::get('apresentacao');
        $curriculo    = Input::file('curriculo');

        $validation = Validator::make(
            array(
                'nome'         => $nome,
                'email'        => $email,
                'apresentacao' => $apresentacao,
                'curriculo'    => $curriculo
            ),
            array(
                'nome'         => 'required',
                'email'        => 'required|email',
                'curriculo'    => 'required|mimes:doc,docx,pdf,txt,rtf,zip|max:4000',
                'apresentacao' => 'required'
            )
        );

        if ($validation->fails())
        {
            $error_msg = '';
            foreach ($validation->messages()->all() as $error) {
                $error_msg .= '<p>'.$error.'</p>';
            }

            $response = array(
                'status'  => 'fail',
                'message' => $error_msg
            );
            return Response::json($response);
        }

        $curriculo_arquivo = $curriculo->getClientOriginalName();
        $curriculo_path    = base_path('curriculos/');

        $object = new Curriculo;
        $object->nome = $nome;
        $object->email = $email;
        $object->telefone = $telefone;
        $object->celular = $celular;
        $object->apresentacao = $apresentacao;
        $object->save();

        $object->curriculo = $object->id.'_'.$curriculo_arquivo;
        $object->download_key = str_random(20).$object->id;
        $object->save();

        $curriculo->move($curriculo_path, $object->id.'_'.$curriculo_arquivo);

        $data = array(
            'nome'         => $nome,
            'email'        => $email,
            'telefone'     => $telefone,
            'celular'      => $celular,
            'apresentacao' => $apresentacao,
            'download_key' => $object->download_key
        );

        Mail::send('emails.curriculo', $data, function($message) use ($data, $contato)
        {
            $message->to('selecao@arezza.com.br', Config::get('projeto.titulo'))
                    ->subject('[CURRÍCULO] '.Config::get('projeto.titulo'))
                    ->replyTo($data['email'], $data['nome']);
        });

        $response = array(
            'status'  => 'success',
            'message' => 'Currículo enviado com sucesso!'
        );
        return Response::json($response);
    }

}
