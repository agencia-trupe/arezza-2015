<?php

class MaoDeObraController extends BaseController {

    private $rotas = [
        'mao-de-obra' => [
            'headcount'               => 'Headcount',
            'trabalho-temporario'     => 'Trabalho temporário',
            'administracao-plena-clt' => 'Administração plena CLT',
            'recrutamento-e-selecao'  => 'Recrutamento e seleção',
            'gestao-onsite'           => 'Gestão onsite'
        ],

        'facilities' => [
            'controle-de-acesso-manutencao-limpeza-e-jardinagem' => 'Controle de acesso, manutenção, limpeza e jardinagem'
        ],

        'marketing-e-promocao' => [
            'marketing-promocional'     => 'Marketing promocional (Trade marketing)',
            'merchandising-e-marketing' => 'Merchandising e Marketing'
        ]
    ];

    private $menu_nome = [
        'mao-de-obra'          => 'Mão de Obra',
        'facilities'           => 'Facilities',
        'marketing-e-promocao' => 'Marketing & Promoção'
    ];

    public function index($slug = null, $interna = null)
    {
        $rotas = $this->rotas;
        $menu_nome = $this->menu_nome;
        if (!$slug) return $this->view('frontend.mao-de-obra.index', compact('rotas', 'menu_nome'));
        if (!array_key_exists($slug, $rotas)) App::abort('404');

        if (!$interna) $interna = array_keys($rotas[$slug])[0];
        if (!array_key_exists($interna, $rotas[$slug])) App::abort('404');

        return $this->view('frontend.mao-de-obra.interna', compact('rotas', 'menu_nome', 'slug', 'interna'));
    }

}
