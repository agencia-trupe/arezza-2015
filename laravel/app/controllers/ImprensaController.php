<?php

use \Imprensa, \ImprensaCategoria;

class ImprensaController extends BaseController {

    public function index($slug = null, $filtro = null)
    {
        if (!$slug) {

            $noticias   = Imprensa::ordenados()->paginate(5);
            $categorias = ImprensaCategoria::get();

            return $this->view('frontend.imprensa.index', compact('noticias', 'categorias', 'filtro'));

        } else {

            $noticia = Imprensa::with('imagens')->slug($slug)->first();
            if (!$noticia) App::abort('404');

            $sidebar    = Imprensa::ordenados()->where('id', '!=', $noticia->id)->limit(5)->get();
            $categorias = ImprensaCategoria::get();

            return $this->view('frontend.imprensa.noticia', compact('noticia', 'categorias', 'sidebar'));

        }
    }

    public function filtro($id)
    {
        $filtro = ImprensaCategoria::find($id);
        if (!$filtro) App::abort('404');

        $noticias   = Imprensa::ordenados()->where('categoria_id', $id)->paginate(5);
        $categorias = ImprensaCategoria::get();

        return $this->view('frontend.imprensa.index', compact('noticias', 'categorias', 'filtro'));
    }

}
