<?php

use \Cases;

class CasesController extends BaseController {

    public function index($id = null)
    {
        $cases = Cases::ordenados()->get(['id', 'titulo']);
        $selecionado = ($id ? Cases::find($id) : Cases::ordenados()->first());
        if (!$selecionado) App::abort('404');

        return $this->view('frontend.cases', compact('cases', 'selecionado'));
    }

}
